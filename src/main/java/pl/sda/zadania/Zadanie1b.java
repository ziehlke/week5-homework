package pl.sda.zadania;

public class Zadanie1b {

    public String crypt(int key, int direction, String str) {
        char[] chars = str.toCharArray();
        int delta = (key * direction) % 26;

        for (int i = 0; i < chars.length; i++) {
            if (isSmallLetter((int) chars[i])) {
                // small letters
                chars[i] = calculateChar("small", chars[i], delta);

            } else if (isCapitalLetter((int) chars[i])) {
                // capital letters
                chars[i] = calculateChar("capital", chars[i], delta);

            } else {
                // char[i] is not a letter - do not change
            }

        }
        return new String(chars);
    }

    public String decrypt(int key, int direction, String str) {
        return crypt(key, -direction, str);
    }


    private char calculateChar(String smallOrCapital, char aChar, int delta) {
        // 65 - 90
        // 97 - 122

        int charInt;
        // taking capital as default
        int lower = 65;
        int upper = 90;

        // change range to small if needed
        if (smallOrCapital.toLowerCase().equals("small")) {
            lower = 97;
            upper = 122;
        }

        charInt = (int)aChar + delta;


        while (charInt < lower) {
            charInt += 26;
        }
        while (charInt > upper) {
            charInt -= 26;
        }

        return (char)charInt;
    }


    private static boolean isCapitalLetter(int i) {
        return i >= 65 && i <= 90;
    }

    private static boolean isSmallLetter(int i) {
        return i >= 97 && i <= 122;
    }


//    public static void main(String[] args) {
//        System.out.println(isSmallLetter((int) 'a'));
//        System.out.println(isSmallLetter((int) 'A'));
//        System.out.println(isCapitalLetter((int) 'a'));
//        System.out.println(isCapitalLetter((int) 'A'));
//        System.out.println("------");
//        System.out.println(isSmallLetter((int) 'b'));
//        System.out.println(isSmallLetter((int) 'B'));
//        System.out.println(isCapitalLetter((int) 'b'));
//        System.out.println(isCapitalLetter((int) 'B'));
//        System.out.println("------");
//        System.out.println(isSmallLetter((int) 'z'));
//        System.out.println(isSmallLetter((int) 'Z'));
//        System.out.println(isCapitalLetter((int) 'z'));
//        System.out.println(isCapitalLetter((int) 'Z'));
//    }

}

package pl.sda.zadania;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Zadanie9 {

    public List removeDuplicates(List list){
        Set set = new HashSet(list);
        List result = new ArrayList();
        result.addAll(set);
        return result;
    }

}

package pl.sda.zadania;

public class Zadanie4 {

    public int calculatePower(int n) {
        int result = n;

        if (n > 1) {
            return (result *= calculatePower(result - 1));
        }

        return 1;
    }
}

package pl.sda.zadania;


import java.util.ArrayList;
import java.util.List;

public class Zadanie7 {

    public int valueAtIndex(int n) {
        List<Integer> a = new ArrayList<>();
        a.add(1); // a[0]
        a.add(1); // a[1]
        a.add(1); // a[2] - size = 3

        while (n >= (a.size())) {
            int tmpThreeN, tmpThreeNNext;

            tmpThreeN = a.get(a.size() - 1) + a.get(a.size() - 2);
            a.add(tmpThreeN);

            tmpThreeNNext = (5 * tmpThreeN) + 4;
            a.add(tmpThreeNNext);
            a.add(tmpThreeNNext);
        }
        return a.get(n);
    }
}

package pl.sda.zadania;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Zadanie10 {

    public void compareLists(List list1, List list2) {

        Set set1 = new HashSet(list1);
        Set set1copy = new HashSet(list1);
        Set set2 = new HashSet(list2);

        set1.removeAll(set2);
        set2.removeAll(set1copy);

        if ( !set1.isEmpty() || !set2.isEmpty() ) {
            System.out.print("TAK");

            if (!set1.isEmpty()) System.out.println(" - na liście 1 są elementy: " + set1 + ", których nie ma na liście 2.");
            if (!set2.isEmpty()) System.out.println(" - na liście 2 są elementy: " + set2 + ", których nie ma na liście 1.");

        }
        else {
            System.out.println("NIE - obie listy mają takie same elementy");
        }

    }

}

package pl.sda.zadania;

import java.util.Scanner;

public class Zadanie3 {

    public void run(){
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Enter a number.\n(must be integer, higher than 2)\n: ");
        int input = Integer.parseInt(scanner.nextLine());


        if ((doTheMath(input) > 0)) {
            System.out.println("the searched number (greatest divider) is:\n" + doTheMath(input));
        } else {
            System.out.println("The given number is prime.");
        }
    }



    private int doTheMath(int n){
        int i = n-1;
        while (i > 1){
            if (n%i == 0){
                return i;
            }
            i--;
        }
        return 0;
    }
}

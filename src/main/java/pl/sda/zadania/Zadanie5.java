package pl.sda.zadania;

public class Zadanie5 {

    public int calculateFibbonacci(int n){
        int result = n;

        if (n > 1) {
            return (result = calculateFibbonacci(result - 1) + calculateFibbonacci(result - 2));
        }

        return 1;


    }

}

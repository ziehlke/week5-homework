package pl.sda.zadania;

import java.util.Stack;

public class Zadanie8 {

    public void printBinary(int number) {

        Stack s = new Stack();

        while ( number > 0) {
            s.push(number%2);
            number /= 2;
        }

        while ( !s.empty()){
            System.out.print(s.pop());
        }

        System.out.println();

    }

}

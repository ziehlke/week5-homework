package pl.sda.zadania;

import java.util.ArrayList;
import java.util.List;

public class Zadanie1 {

    public String crypt(String stringToBeCrypted) {
        char[] chars = stringToBeCrypted.toCharArray();
        String result = "";


        for (int i = 0; i < chars.length; i += 2) {
            result += chars[i];
        }

        for (int i = 1; i < chars.length; i += 2) {
            result += chars[i];
        }

        return result;
    }


    public String decrypt(String stringToBeDecrypted) {

        String str1, str2, result;
        result = "";

        if (stringToBeDecrypted.length() % 2 == 1) {
            str1 = stringToBeDecrypted.substring(0, (stringToBeDecrypted.length() / 2) + 1);
            str2 = stringToBeDecrypted.substring(((stringToBeDecrypted.length()) / 2) +1, stringToBeDecrypted.length());
        } else {
            str1 = stringToBeDecrypted.substring(0, (stringToBeDecrypted.length() / 2));
            str2 = stringToBeDecrypted.substring((stringToBeDecrypted.length() / 2), stringToBeDecrypted.length());
        }

        for (int i = 0; i < str1.length(); i++) {

            result += str1.charAt(i);

            if (str2.length() > i) {
                result += str2.charAt(i);
            }

        }

        return result;
    }

}

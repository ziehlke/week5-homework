package pl.sda.zadania;

public class Zadanie11 {


    public void cryptWithKey(String string, String key) {

        char[] chars = string.toCharArray();
        char[] keyChars = key.toCharArray();


        for (int i = 0; i < chars.length; i++) {
            if (isSmallLetter((int) chars[i])) {
                // small letters
                chars[i] = calculateChar("small", chars[i], keyChars[i % keyChars.length]);

            } else if (isCapitalLetter((int) chars[i])) {
                // capital letters
                chars[i] = calculateChar("capital", chars[i], keyChars[i % keyChars.length]);

            } else {
                // char[i] is not a letter - do not change
            }

        }

        String str = new String(chars);
        System.out.println(str);
    }

    private char calculateChar(String smallOrCapital, char aChar, char keyChar) {

        int lower = 65;
        int upper = 90;

        // change range to small if needed
        if (smallOrCapital.toLowerCase().equals("small")) {
            lower = 97;
            upper = 122;
        }


        aChar += keyChar;


        while (aChar < lower) {
            aChar += 26;
        }
        while (aChar > upper) {
            aChar -= 26;
        }

        return aChar;
    }


    private static boolean isCapitalLetter(int i) {
        return i >= 65 && i <= 90;
    }

    private static boolean isSmallLetter(int i) {
        return i >= 97 && i <= 122;
    }

}

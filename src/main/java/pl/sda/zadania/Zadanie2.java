package pl.sda.zadania;

import java.util.Arrays;

public class Zadanie2 {

//TODO: crypt = decrypt ? generalnie to chyba nie dziala z innym kluczem niz 2:-P
//
//    public String crypt(String toCrypt) {
//        char[] chars = toCrypt.toCharArray();
//        char[] result = new char[chars.length];
//        result[0] = chars[0];
//
//        for (int i = 1; i < chars.length; i += 2) {
//            result[i] = chars[i + 1];
//        }
//        for (int i = 2; i < chars.length; i += 2) {
//            result[i] = chars[i - 1];
//        }
//        return new String(result);
//    }



    public String decrypt(String toDecrypt) {
        char[] chars = toDecrypt.toCharArray();
        char[] result = new char[chars.length];
        result[0] = chars[0];

        for (int i = 1; i < chars.length; i += 2) {
            result[i] = chars[i + 1];
        }
        for (int i = 2; i < chars.length; i += 2) {
            result[i] = chars[i - 1];
        }
        return new String(result);
    }

    public String crypt(String line, int start, int width){
        char[] arr = line.toCharArray();

        for (int i = start; i < arr.length-width; i+= width) {
            char[] subarr = Arrays.copyOfRange(arr,i,i+width);

            for (int j=0; j < subarr.length; j++){
                int putIndex = j+1;
                putIndex = putIndex % width;

                arr[i+putIndex]= subarr[j];
            }
        }
        return new String(arr);
    }



    public String decrypt(String line, int start, int width){
        char[] arr = line.toCharArray();
        int absWidth = Math.abs(width);

        for (int i = start; i < arr.length - (absWidth - 1); i += absWidth){
            char[] subarr = Arrays.copyOfRange(arr, i, i+absWidth);

            for (int j=0; j < subarr.length; j++){
                int putIndex = j + (width > 0 ? 1 : -1);
                putIndex = putIndex % absWidth;

                if (putIndex < 0) {
                    putIndex += absWidth;
                }
                arr[i+putIndex] = subarr[j];
            }
        }
        return new String(arr);
    }
}
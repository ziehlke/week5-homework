package pl.sda.zadania;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Runner {
    public static void main(String[] args) {




        System.out.println("---------------------ZADANIE 1----------------------------------");
        Zadanie1 zad1 = new Zadanie1();
//        System.out.println(zad1.crypt("12345678"));
//        System.out.println(zad1.decrypt("13572468"));
//
//        System.out.println(zad1.crypt("123456789"));
//        System.out.println(zad1.decrypt("135792468"));

        System.out.println("---------------------ZADANIE 1b----------------------------------");
        Zadanie1b zad1b = new Zadanie1b();
//        System.out.println(zad1b.crypt(1,1,"abcABC"));
//        System.out.println(zad1b.crypt(1,1,"xyzXYZ"));
//        System.out.println(zad1b.crypt(25,1,"abcABC"));
//        System.out.println(zad1b.crypt(26,1,"abcABC"));
//        System.out.println(zad1b.crypt(1,-1,"xyzXYZ"));
//        System.out.println(zad1b.crypt(1,-1,"abcABC"));
//        System.out.println(zad1b.crypt(25,-1,"abcABC"));
//        System.out.println(zad1b.crypt(26,-1,"abcABC"));
//        System.out.println(zad1b.crypt(2,1,"abcdefg"));
//        System.out.println(zad1b.decrypt(2,1,"cdefghi"));


        System.out.println("---------------------ZADANIE 2----------------------------------");
        Zadanie2 zadanie2 = new Zadanie2();
//        System.out.println(zadanie2.crypt("AleksanderKwasniewski"));
//        System.out.println(zadanie2.decrypt("AelsknaedKrawnseiswik"));
//        System.out.println("tu jest jakis feler - chyba nie dziala dla kluczy wiecej niz 2");
//        System.out.println(zadanie2.crypt("AleksanderKwasniewski",1,2));
//        System.out.println(zadanie2.decrypt("AelsknaedKrawnseiswik",1,2));

        System.out.println("---------------------ZADANIE 3----------------------------------");
        Zadanie3 zadanie3 = new Zadanie3();
//        zadanie3.run();

        System.out.println("---------------------ZADANIE 4----------------------------------");
        Zadanie4 zadanie4 = new Zadanie4();
//        System.out.println(zadanie4.calculatePower(1));
//        System.out.println(zadanie4.calculatePower(2));
//        System.out.println(zadanie4.calculatePower(3));
//        System.out.println(zadanie4.calculatePower(4));
//        System.out.println(zadanie4.calculatePower(5));
//        System.out.println(zadanie4.calculatePower(6));


        System.out.println("---------------------ZADANIE 5----------------------------------");
        Zadanie5 zadanie5 = new Zadanie5();
//        System.out.println(zadanie5.calculateFibbonacci(1));
//        System.out.println(zadanie5.calculateFibbonacci(2));
//        System.out.println(zadanie5.calculateFibbonacci(3));
//        System.out.println(zadanie5.calculateFibbonacci(4));
//        System.out.println(zadanie5.calculateFibbonacci(5));
//        System.out.println(zadanie5.calculateFibbonacci(6));
//        System.out.println(zadanie5.calculateFibbonacci(7));
//        System.out.println(zadanie5.calculateFibbonacci(8));
//        System.out.println(zadanie5.calculateFibbonacci(9));
//        System.out.println(zadanie5.calculateFibbonacci(10));

        System.out.println("---------------------ZADANIE 6----------------------------------");
        Zadanie6 zadanie6 = new Zadanie6();
        int[] test = {0,1,1,1,4,5,6,7,8,9};
//        System.out.println(zadanie6.searchByDivision(test, 0));
//        System.out.println(zadanie6.searchByDivision(test, 1));
//        System.out.println(zadanie6.searchByDivision(test, 2));
//        System.out.println(zadanie6.searchByDivision(test, 3));
//        System.out.println(zadanie6.searchByDivision(test, 4));
//        System.out.println(zadanie6.searchByDivision(test, 5));
//        System.out.println(zadanie6.searchByDivision(test, 6));
//        System.out.println(zadanie6.searchByDivision(test, 7));
//        System.out.println(zadanie6.searchByDivision(test, 8));
//        System.out.println(zadanie6.searchByDivision(test, 9));


        System.out.println("---------------------ZADANIE 7----------------------------------");
        Zadanie7 zadanie7 = new Zadanie7();
//        System.out.println(zadanie7.valueAtIndex(3));
//        System.out.println(zadanie7.valueAtIndex(4));
//        System.out.println(zadanie7.valueAtIndex(5));
//        System.out.println(zadanie7.valueAtIndex(6));
//        System.out.println(zadanie7.valueAtIndex(7));
//        System.out.println(zadanie7.valueAtIndex(8));
//        System.out.println(zadanie7.valueAtIndex(9));
//        System.out.println(zadanie7.valueAtIndex(10));


        System.out.println("---------------------ZADANIE 8----------------------------------");
        Zadanie8 zadanie8 = new Zadanie8();
//        zadanie8.printBinary(1);
//        zadanie8.printBinary(2);
//        zadanie8.printBinary(3);
//        zadanie8.printBinary(4);
//        zadanie8.printBinary(5);
//        zadanie8.printBinary(6);
//        zadanie8.printBinary(7);
//        zadanie8.printBinary(8);
//        zadanie8.printBinary(9);


        System.out.println("---------------------ZADANIE 9----------------------------------");
        Zadanie9 zadanie9 = new Zadanie9();
//        List<Integer> mylist = new ArrayList<>(Arrays.asList(1,1,2,3,2,2,1,3,4,2,5,2,3));
//        System.out.println(zadanie9.removeDuplicates(mylist));
//        System.out.println(zadanie9.removeDuplicates(mylist).getClass());

        System.out.println("---------------------ZADANIE 10---------------------------------");
        Zadanie10 zadanie10 = new Zadanie10();
//
//        List<Integer> lista1 = new ArrayList<>(Arrays.asList(1, 2, 1, 1, 2, 2, 3, 3, 1, 1, 1, 1, 2));
//        List<Integer> lista2 = new ArrayList<>(Arrays.asList(1, 2, 3));
//        zadanie10.compareLists(lista1,lista2);
//
//        lista1 = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 1, 1, 2, 2, 3);
//        lista2 = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
//        zadanie10.compareLists(lista1,lista2);
//
//        lista1 = Arrays.asList(1, 2, 3);
//        lista2 = Arrays.asList(4, 5, 6);
//        zadanie10.compareLists(lista1,lista2);

        System.out.println("---------------------ZADANIE 11---------------------------------");

        Zadanie11 zadanie11 = new Zadanie11();
//        zadanie11.cryptWithKey("szyfrowanie","klucz");

    }
}

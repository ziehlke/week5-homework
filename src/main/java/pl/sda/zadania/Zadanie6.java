package pl.sda.zadania;

public class Zadanie6 {

    public int searchByDivision(int[] list, int searched) {

        int from = 0;
        int to = list.length - 1;
        int middle = (from + to) / 2;

        while (searched != list[middle] && searched != list[to] && from <= to) {
            if (list[middle] < searched) from = middle + 1;
            else to = middle - 1;
            middle = (from + to) / 2;
        }
        if (to < from) return -1;
        return (searched == list[middle]) ? middle : to;
    }
}